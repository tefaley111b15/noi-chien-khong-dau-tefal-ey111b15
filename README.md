Thông tin chi tiết Nồi chiên không dầu Tefal EY111B15:

Tham khảo: https://hiyams.com/noi-dien/noi-chien-khong-dau/noi-chien-khong-dau-tefal/noi-chien-khong-dau-tefal-ey111b15/

Nồi chiên không dầu Tefal EY111B15 ứng dụng công nghệ khí nóng đa chiều. Không cần đảo trong quá trình nấu, nhưng thức ăn vẫn chín đều, giòn, ngon.

Nồi chiên không dầu Tefal EY111B15 dung tích nồi 4.2 lít, thích hợp để chế biến thức ăn cho 6 người. Vỏ ngoài của nồi được làm bằng nhựa có khả năng chịu nhiệt cao.

Nồi chiên không dầu Tefal EY111B15 được thiết lập sẵn 8 chương trình: khoai tây chiên, bánh bông lan, pizza, bánh mì nướng, gà, các loại thịt, tôm, cá. Giúp quá trình nấu ăn trở nên đơn giản và tiết kiệm thời gian.

Thông số kỹ thuật Nồi chiên không dầu Tefal EY111B15:

Công suất: 1600 W

Bảng điều khiển: Cảm ứng, đèn sáng mỗi chức năng, dễ sử dụng và thao tác

Cộng nghệ: Công nghệ khí nóng đa chiều, không cần đảo khi nấu

Chức năng: 8 chức năng cài đặt sẵn (khoai tây chiên, bánh bông lan, pizza, bánh mì nướng, gà, các loại thịt, tôm, cá)

Chất liệu lòng nồi: Bằng thép không gỉ phủ chống dính

Vỏ ngoài: Bằng nhựa chịu nhiệt cao

Tính năng: Có khả năng tự động ngắt điện, chuông báo khi thức ăn đã chín

Hẹn giờ: Lên đến 60 phút

Nhiệt độ nấu: Nhiệt độ lên đến 200 độ C

Dung tích: Lớn, dùng được cho 6 người, 4.2L

Màu sắc: Bạc

Xuất xứ: Trung Quốc

Kích thước: 33.2 x 33.2 x 35.0 cm (DxRxC)

Khối lượng: 3.85kg

Bảo hành: 2 năm